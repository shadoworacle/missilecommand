class MissileController
{
  //main variables
  PVector targetPosition = new PVector(); //Pvector for target position
  PVector currentPosition = new PVector(); //Pvector for current position
  float launchPointX, launchPointY;
  float speed; //speed of player missile
  boolean isEnemy; //boolean for if the missiles is an enemies or not 
  PImage target; //image variable for the target cursor

  //explosion variables
  boolean isExploding = false; 
  boolean reachedPeakExplosion = false;
  float explosionSize = 0; //initial explosion size
  float explosionTotalSize = 40; //total explosion size 


  MissileController(PVector currentPos, PVector targetPos, float _Speed, boolean _isEnemy)//currentPos is where the missile comes from mainly the Y, the targetPos is where the missile will hit, the _launchPoint is where it will launch on the X
  {
    currentPosition = currentPos; 
    targetPosition = targetPos;
    launchPointX = currentPosition.x; //current x position for missile
    launchPointY = currentPosition.y; //current y position for missile
    speed = _Speed; //speed of missile
    isEnemy = _isEnemy; //checks if the missile is a player or enemy missile
    target = loadImage("data/sprites/cursor-target.png");
  }



  void Display() //change display of missile depending on it's state
  {
    if (isEnemy)
    {
      fill(255, 0, 0);   
      stroke(255, 0, 0);
    } else
    {
      if (!isExploding)
      {
        image(target, targetPosition.x, targetPosition.y);
      }

      fill(0, 0, 255);
      stroke(0, 0, 255);
    }

    if (isExploding) //changes missile properties if missile is exploding
    {  
      ellipseMode(RADIUS);       
      ellipse(currentPosition.x, currentPosition.y, explosionSize, explosionSize); //creates radius for the explosion
      if (explosionSize > 28)
      {
        reachedPeakExplosion = true;
      }
      if (reachedPeakExplosion)
      {
        explosionSize = lerp(explosionSize, 0, 0.05);
        return;
      }
      explosionSize = lerp(explosionSize, explosionTotalSize, 0.1); //linear interpolation for size of explosion 

      return;
    }

    rectMode(CENTER);

    rect(currentPosition.x, currentPosition.y, 20, 20);

    line(launchPointX, launchPointY, currentPosition.x, currentPosition.y); // line for missile trajectory
  }

  void Update() //update all missiles
  {
    if (isExploding) return;

    if (currentPosition.dist(targetPosition) <= 1) //when missile reaches target location play this if statement for explosion
    {
      isExploding = true;
      boom.play(); //Play the boom sound if the missile changes state to exploding
    }

    if (isEnemy)//this controls the enemy missile
    {
      PVector dir = new PVector(targetPosition.x - currentPosition.x, targetPosition.y - currentPosition.y); //calculates the direction using target position and current position 

      if (dir.mag() > 0.0) {
        dir.normalize();
        dir.mult(min(speed, dir.mag()));

        currentPosition.x += dir.x;
        currentPosition.y += dir.y;
      }
    } else //this is the player missile 
    {
      currentPosition.x = lerp(currentPosition.x, targetPosition.x, speed);
      currentPosition.y = lerp(currentPosition.y, targetPosition.y, speed);
    }
  }
}

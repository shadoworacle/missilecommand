class PlayerMissile
{
  MissileController missile; //instantiate player missile instance

  PlayerMissile(PVector target, PVector launchPoint) //Pvector for player missiles 
  {
    missile = new MissileController(launchPoint, target, 0.1, false);
  }

  void Update() //update state of missiles 
  {
    missile.Update();
  }

  void Display() //display missiles
  {
    missile.Display();
  }
}

import processing.sound.*;



SoundFile welcome;
SoundFile boom;
SoundFile pew;
SoundFile gameOver;
SoundFile congrats;
/*
 boom.play();
 pew.play();
 gameOver.play();
 congrats.play();
 */

GameManager gm;

void setup()
{
  size(1920, 1080);
  // Sound import 
  welcome = new SoundFile(this, "data/sounds/welcome.wav"); 
  boom = new SoundFile(this, "data/sounds/boom.wav"); 
  pew = new SoundFile(this, "data/sounds/pew.wav");
  gameOver = new SoundFile(this, "data/sounds/game over.wav");
  congrats = new SoundFile(this, "data/sounds/congrats.wav");
  gm = new GameManager();
}

void draw()
{
  gm.Update();
}

void mousePressed() //Check if mouse is pressed on the title screen, switch to game state
{
  if (gm.statemachine == 1)
  {
    gm.statemachine = 2;
  } else if (gm.statemachine == 2)
  {
    gm.SiloLaunchSystem();
    pew.play();
  } else if (gm.statemachine == 3)
  {
    gm.statemachine = 1;
  }
}

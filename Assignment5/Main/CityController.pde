class City {
  PVector position1=new PVector(); //coordinates for x1, y1 position
  PVector positionSize=new PVector(); //coordinates for width and height
  boolean localcityState; //state of the city instance
  float w = 32;
  float h = 16;
  PImage city;


  City(float x1, float y1, boolean cityState) { //calculate position for hitbox of city 
    position1.x=x1; 
    position1.y=y1;
    //    positionSize.x=w;
    //     positionSize.y=h;
    localcityState = cityState;
    city = loadImage("data/sprites/city.png");
  }

  void cityStateSwitch() { //set state of the city Alive/Dead
    localcityState = !localcityState;
  }

  void Display() {
    if (localcityState) {
      image(city, position1.x, position1.y, 64, 32); //set width and height of the final sprite 
    }
  }
}

Score_System score; //game state controller

void titleScreen() { //sets state to title screen, draws title of game

  fill(#B70000);

  textSize(220); //Sets large text font
  text("MISSILE", 550, 300); //centered text for 'missile command' title
  text("COMMAND", 360, 500);

  rect(720, 750, 400, 400);

  fill(#FFFFFF);

  textSize(80);
  text("CLICK", 800, 850); //centered text for 'Click to start' 
  text("TO", 860, 930);
  text("START", 800, 1000);
}

void gameScreen() {
}

void pointScreen() { //sets state to points screen

  fill(#B70000);

  textSize(220);
  text("LEVEL", 660, 300); //centered text for 'level complete' text
  text("COMPLETE", 400, 500);
}

void titleButton() {
}

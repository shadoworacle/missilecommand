class EnemyMissile {
  boolean isAlive;

  PVector position = new PVector(); //PVector for the position of the enemy missile
  PVector velocity = new PVector(); //PVector for velocity of enemy missile

  PVector target = new PVector(); //PVector for target position for enemy missile 

  MissileController missile; //instantiates missile instances

  EnemyMissile(PVector target_) { //sets enemy missile variables to those of the PVector target_
    target = target_;
    float xPos = random(0, width); //set random variable to x position of missile upon spawn  
    missile = new MissileController(new PVector(xPos, 0), target_, 1, true);
  }

  //displays EnemyMissile
  void Display() { 
    missile.Display();
  }


  void Update() { //updates Enemy missile 

    missile.Update();
  }
}

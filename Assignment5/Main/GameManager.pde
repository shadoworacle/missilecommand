class GameManager
{
  ArrayList<EnemyMissile> enemyMissile = new ArrayList<EnemyMissile>(); //Instantiate array for enemy missiles
  ArrayList<EnemyMissile> explodedEnemy = new ArrayList<EnemyMissile>(); //instantiate array for exploded enemies 
  ArrayList<PlayerMissile> playerMissile = new ArrayList<PlayerMissile>(); //instantiate array for player missiles 
  float amountToSpawn;

  int siloAmmo1 = 10; //Amount of ammo for each Silo at start of game

  int siloAmmo2 = 10;

  int siloAmmo3 = 10;

  PImage cursor, base1, base2, base3; //declare variables for individual images

  int statemachine;  //1: title state, 2: game state, 3: point state

  int score; //Initiate score variable 


  GameManager() //Main game manager that is called upon in main
  {
    // loading all of the images
    cursor = loadImage("data/sprites/cursor.png"); 

    base1 = loadImage("data/sprites/base.png");
    base2 = loadImage("data/sprites/base.png");
    base3 = loadImage("data/sprites/base.png");

    noCursor();

    welcome.play(); //sound that plays at the begining of the game


    imageMode(CENTER);
    Initialize();
  }

  void Update() //update state
  {
    backgroundLogic();

    if (statemachine == 1)//title state
    {

      Initialize();
      titleScreenState();
    } else if (statemachine == 2)//game state
    {
      GameState();
    } else if (statemachine == 3) //points
    {
      PointScreen();
    } else
    {
      statemachine = 1;
    }

    if (enemyMissile.size() <= 0)
    {
      statemachine = 3;
    }

    image(cursor, mouseX, mouseY);
  }

  void backgroundLogic() //Setup for background effects
  {
    //  background(0);

    //stars, uses manipulation of the lack of a true background to create fading stars effect
    fill(0, 50);
    rect(0, 0, width*2, height*2);
    noStroke();
    fill(random(0, 255), random(0, 255));
    ellipse(random(width), random(height), 4, 4);
    ellipse(random(width), random(height), 4, 4);
    ellipse(random(width), random(height), 4, 4);

    // background(0);
    stroke(255, 0, 0);
  }

  void Initialize() 
  {
    siloAmmo1 = 10; //ammo count
    siloAmmo2 = 10;
    siloAmmo3 = 10;
    for (int i = enemyMissile.size() - 1; i >= 0; i--) { //removes enemy missile from the array
      enemyMissile.remove(i);
    }
    for (int i = explodedEnemy.size() - 1; i >= 0; i--) { //remove exploded enemy from array
      explodedEnemy.remove(i);
    }
    for (int i = playerMissile.size() - 1; i >= 0; i--) { //remove playermissile from array
      playerMissile.remove(i);
    }

    EnemyMissileSpawn(random(1, 15)); //Spawn a random amount of enemy missiles from 1 to 15
  }

  void titleScreenState() //State for the title screen
  {
    fill(#B70000);
    //Drawing text for title screen.
    textSize(220);
    text("MISSILE", 580, 300);
    text("COMMAND", 460, 500);


    fill(#FFFFFF);

    textSize(80);
    text("CLICK", 800, 850);
    text("TO", 860, 930);
    text("START", 800, 1000);
  }

  void GameState()
  {
    //UI box
    fill(80, 30, 50, 380);
    stroke(255, 0, 0);
    rectMode(CORNERS);
    rect(-100, 900, 1920, 1080);
    rectMode(CENTER);

    //City drawing
    City CC; 
    CC = new City(1150, 897, true); //City instance
    CC.Display();
    City CC2;
    CC2 = new City(750, 897, true);
    CC2.Display();
    City CC3;
    CC3 = new City(500, 897, true);
    CC3.Display();
    City CC4;
    CC4 = new City(1400, 897, true);
    CC4.Display();
    City CC5;
    CC5 = new City(1800, 897, true);
    CC5.Display();
    City CC6;
    CC6 = new City(150, 897, true);
    CC6.Display();


    for (EnemyMissile enemy : enemyMissile) //display and update missile instances
    {
      enemy.Update();
      enemy.Display();
    }

    for (PlayerMissile missile : playerMissile) //display and update player missile instances 
    {
      missile.Update();
      missile.Display();
      MissileCollision(missile);
    }

    for (EnemyMissile enemy : explodedEnemy) //display and update exploded enemy instances 
    {
      enemy.Update();
      enemy.Display();
      EnemyExplodeEnemy(enemy);
    }
    //Set the size of the images to the proper scale for our resolution
    image(base1, 320, 900, 128, 64); 
    image(base2, width/2, 900, 128, 64);
    image(base3, 1600, 900, 128, 64);
    textSize(32);
    fill(0, 0, 255);
    text(siloAmmo1, 305, 910);
    text(siloAmmo2, width/2 - 15, 910);
    text(siloAmmo3, 1600 - 15, 910);
  }



  void PointScreen() //pointscreen function
  {
    score = siloAmmo1 + siloAmmo2 + siloAmmo3; //score calculation system
    fill(#B70000);
    textSize(220);
    text("LEVEL", 660, 300);
    text("COMPLETE", 400, 500);
    textSize(100);
    text("SCORE: " + score, 700, 700); //display score acheived on the title screen

    //+ score1.ScoreCounter()
  }

  boolean collide(float x1, float y1, float r1, float x2, float y2, float r2) { //collision detection
    if (dist(x1, y1, x2, y2) < r1 + r2) {
      return true;
    } else {
      return false;
    }
  }

  void MissileCollision(PlayerMissile missile) //collision detection for player missiles 
  {
    EnemyMissile enemy;
    for (int i = 0; i <= enemyMissile.size() - 1; i++)
    {
      enemy = enemyMissile.get(i);
      if ((collide(missile.missile.currentPosition.x, missile.missile.currentPosition.y, 20, enemy.missile.currentPosition.x, enemy.missile.currentPosition.y, 20)) ||
        (collide(missile.missile.currentPosition.x, missile.missile.currentPosition.y, missile.missile.explosionSize, enemy.missile.currentPosition.x, enemy.missile.currentPosition.y, 20))) //||
      {
        score += 5;
        missile.missile.isExploding = true;
        enemy.missile.isExploding = true;
        boom.play();
        explodedEnemy.add(enemy);
        enemyMissile.remove(enemy);
      }
    }
  }

  void EnemyExplodeEnemy(EnemyMissile explosion) //sets the state of an enemy or player missile to exploding
  {
    for (int i = 0; i <= enemyMissile.size() - 1; i++)
    {
      if ((collide(explosion.missile.currentPosition.x, explosion.missile.currentPosition.y, explosion.missile.explosionSize, enemyMissile.get(i).missile.currentPosition.x, enemyMissile.get(i).missile.currentPosition.y, 20)))
      {
        score += 5;
        enemyMissile.get(i).missile.isExploding = true;
        //explodedEnemy.add(enemyMissile.get(i));

        enemyMissile.remove(enemyMissile.get(i));
      }
    }
  }


  void EnemyMissileSpawn(float amountOfEnemies) { //spawn enemy missiles
    amountToSpawn = amountOfEnemies; //variable for amount of enemies to spawn 
    for (int i = 0; i <= amountToSpawn - 1; i++) {
      enemyMissile.add(new EnemyMissile(new PVector(random(0, width), 900)));
    }
  }


  void SiloLaunchSystem()//controls where the missiles will be launched from and the ammo
  {
    PVector silo1 = new PVector(320, 900);
    PVector silo2 = new PVector(width/2, 900);
    PVector silo3 = new PVector(1600, 900);
    PVector currentMousePos = new PVector(mouseX, mouseY);

    //check which silo to fire missile from
    float siloDist1 = currentMousePos.dist(silo1); 
    float siloDist2 = currentMousePos.dist(silo2);
    float siloDist3 = currentMousePos.dist(silo3);

    float[] siloDists = {siloDist1, siloDist2, siloDist3};

    float minSiloDist = min(siloDists); //calculates the distance for Silos 

    float targetSilo = 1;

    for (int i = 0; i <= 2; i++)
    {
      if (minSiloDist == siloDists[i])
      {
        targetSilo = i + 1;
      }
    }

    if (targetSilo == 1)  //gets missiles from target silo 1 if within it's range, nested if/else sets which silo is being fired from
    {
      if (siloAmmo1 >= 1) {
        siloAmmo1 -= 1;
        playerMissile.add(new PlayerMissile(new PVector(mouseX, mouseY), silo1));
      } else
      {
        if (siloAmmo2 >= 1)
        {
          siloAmmo2 -= 1;
          playerMissile.add(new PlayerMissile(new PVector(mouseX, mouseY), silo2));
        } else if (siloAmmo3 >= 1)
        {
          siloAmmo3 -= 1;
          playerMissile.add(new PlayerMissile(new PVector(mouseX, mouseY), silo3));
        }
      }
    } else if (targetSilo == 2)
    {
      if (siloAmmo2 >= 1)
      {
        siloAmmo2 -= 1;//
        playerMissile.add(new PlayerMissile(new PVector(mouseX, mouseY), silo2));
      } else
      {
        if (siloAmmo1 >= 1) {
          siloAmmo1 -= 1;
          playerMissile.add(new PlayerMissile(new PVector(mouseX, mouseY), silo1));
        } else if (siloAmmo3 >= 1)
        {
          siloAmmo3 -= 1;
          playerMissile.add(new PlayerMissile(new PVector(mouseX, mouseY), silo3));
        }
      }
    } else if (targetSilo == 3)
    {
      if (siloAmmo3 >= 1)
      {
        siloAmmo3 -= 1;
        playerMissile.add(new PlayerMissile(new PVector(mouseX, mouseY), silo3));
      } else
      {
        if (siloAmmo2 >= 1)
        {
          siloAmmo2 -= 1;
          playerMissile.add(new PlayerMissile(new PVector(mouseX, mouseY), silo2));
        } else if (siloAmmo1 >= 1) {
          siloAmmo1 -= 1;
          playerMissile.add(new PlayerMissile(new PVector(mouseX, mouseY), silo1));
        }
      }
    }
  }
}

class Score_System {

  float localCityCount; //Local variable for citycount, counts remaining cities at end of level
  float localUnusedMissileCount; //local variable for UnusedMissileCount, counts remaining missiles at end of level
  float ScoreTotal; //Variable to store the total score, to be displayed on the score screen

  Score_System(float CityCount, float UnusedMissileCount) { //move information from score_system constructor into the local class
    localCityCount = CityCount;
    localUnusedMissileCount = UnusedMissileCount;
  }

  float ScoreCounter() { //counts the total score
    ScoreTotal += (localCityCount*25) + (localUnusedMissileCount*5);
    return ScoreTotal; //returns the total score
  }
}
